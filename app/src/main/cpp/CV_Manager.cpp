//
// Created by AL INFOS on 23/01/2024.
//

#include <opencv2/imgproc/types_c.h>
#include "CV_Manager.h"
#include "soket_client.h"
#include "Encoder.h"

using namespace std;
using namespace cv;


//MediaCodec pour l'encodage et le décodage vidéo H264 sur Android.
//Conversion

void convertYUV_I420toNV12(unsigned char* i420bytes, unsigned char* nv12bytes, int width, int height)

{

    int nLenY = width * height;

    int nLenU = nLenY / 4;

    memcpy(nv12bytes, i420bytes, width * height);

    for (int i = 0; i < nLenU; i++) {

        nv12bytes[nLenY + 2 * i] = i420bytes[nLenY + i];             // U

        nv12bytes[nLenY + 2 * i + 1] = i420bytes[nLenY + nLenU + i]; // V

    }

}



void BGR2YUV_nv12(cv::Mat &src, cv::Mat &dst)

{

    int w_img = src.cols;

    int h_img = src.rows;

    dst = cv::Mat(h_img*1.5, w_img, CV_8UC1, cv::Scalar(0));

    cv::Mat YUV_I420(h_img*1.5, w_img, CV_8UC1, cv::Scalar(0));  //YUV_I420

    cv::cvtColor(src, YUV_I420, cv::COLOR_BGR2YUV_I420);

    convertYUV_I420toNV12(YUV_I420.data, dst.data, w_img, h_img);

}
CV_Manager::CV_Manager()
        : m_camera_ready(false), m_image(nullptr), m_image_reader(nullptr),
          m_native_camera(nullptr), audioBuffer{} {
}


void recorderCallback(SLAndroidSimpleBufferQueueItf bq, void *context) {
    // Cast contexte à CV_Manager* pour accéder aux membres de la classe
    CV_Manager* cvManager = static_cast<CV_Manager*>(context);

    // Vérification de la validité de l'instance CV_Manager
    if (cvManager == nullptr) {
        // Gérer l'erreur
        return;
    }

    // Taille du tampon audio
    const int bufferSize = cvManager->GetBufferSize();

    // Ouvrir le fichier pour enregistrer les données audio
    FILE *audioFile = fopen("audio_data.pcm", "ab");
    if (!audioFile) {
        // Gérer l'erreur d'ouverture de fichier
        return;
    }

    // Lire les données audio depuis le tampon et les écrire dans le fichier
    fwrite(cvManager->GetAudioBuffer(), sizeof(short), bufferSize, audioFile);

    // Fermer le fichier
    fclose(audioFile);

    // Lancer l'enregistrement du prochain tampon
    (*cvManager->GetRecorderBufferQueue())->Enqueue(cvManager->GetRecorderBufferQueue(), cvManager->GetAudioBuffer(), bufferSize * sizeof(short));
}


void CV_Manager::SetUpAudioRecording() {
    // Création de l'engine audio
    slCreateEngine(&engineObject, 0, nullptr, 0, nullptr, nullptr);
    (*engineObject)->Realize(engineObject, SL_BOOLEAN_FALSE);
    (*engineObject)->GetInterface(engineObject, SL_IID_ENGINE, &engineEngine);

    // Configuration de la source audio
    SLDataLocator_IODevice loc_dev = {SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT,
                                      SL_DEFAULTDEVICEID_AUDIOINPUT, nullptr};
    SLDataSource audioSource = {&loc_dev, nullptr};

    // Configuration du format audio
    SLDataLocator_AndroidSimpleBufferQueue loc_bq = {
            SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, NUM_CHANNELS,
                                   SL_SAMPLINGRATE_44_1, SL_PCMSAMPLEFORMAT_FIXED_16,
                                   SL_PCMSAMPLEFORMAT_FIXED_16, SL_SPEAKER_FRONT_LEFT,
                                   SL_BYTEORDER_LITTLEENDIAN};
    SLDataSink audioSink = {&loc_bq, &format_pcm};

    // Création de l'enregistreur audio
    const SLInterfaceID ids[1] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE};
    const SLboolean req[1] = {SL_BOOLEAN_TRUE};
    (*engineEngine)->CreateAudioRecorder(engineEngine, &recorderObject, &audioSource,
                                         &audioSink, 1, ids, req);
    (*recorderObject)->Realize(recorderObject, SL_BOOLEAN_FALSE);
    (*recorderObject)->GetInterface(recorderObject, SL_IID_RECORD, &recorderRecord);
    (*recorderObject)->GetInterface(recorderObject, SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
                                    &recorderBufferQueue);
    (*recorderBufferQueue)->RegisterCallback(recorderBufferQueue, recorderCallback, nullptr);
}

void CV_Manager::StartAudioRecording() {
    (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_RECORDING);
    // Commencez à lire les buffers audio
    (*recorderBufferQueue)->Enqueue(recorderBufferQueue, audioBuffer, BUFFER_SIZE);
}

void CV_Manager::StopAudioRecording() {
    (*recorderRecord)->SetRecordState(recorderRecord, SL_RECORDSTATE_STOPPED);
}

void CV_Manager::ReleaseAudioRecorder() {
    if (recorderObject != nullptr) {
        (*recorderObject)->Destroy(recorderObject);
        recorderObject = nullptr;
        recorderRecord = nullptr;
        recorderBufferQueue = nullptr;
    }
    if (engineObject != nullptr) {
        (*engineObject)->Destroy(engineObject);
        engineObject = nullptr;
        engineEngine = nullptr;
    }
}

CV_Manager::~CV_Manager() {
    // ACameraCaptureSession_stopRepeating(m_capture_session);
    if (m_native_camera != nullptr) {
        delete m_native_camera;
        m_native_camera = nullptr;
    }

    // make sure we don't leak native windows
    if (m_native_window != nullptr) {
        ANativeWindow_release(m_native_window);
        m_native_window = nullptr;
    }

    if (m_image_reader != nullptr) {
        delete (m_image_reader);
        m_image_reader = nullptr;
    }
    /* StopAudioRecording();
    ReleaseAudioRecorder(); */
}

void CV_Manager::SetNativeWindow(ANativeWindow *native_window) {
    // Save native window
    m_native_window = native_window;
}

void CV_Manager::SetUpCamera() {

    m_native_camera = new Native_Camera(m_selected_camera_type);

    m_native_camera->MatchCaptureSizeRequest(&m_view,
                                             ANativeWindow_getWidth(m_native_window),
                                             ANativeWindow_getHeight(m_native_window));

    ASSERT(m_view.width && m_view.height, "Could not find supportable resolution");

    // Here we set the buffer to use RGBX_8888 as default might be; RGB_565
    ANativeWindow_setBuffersGeometry(m_native_window, m_view.width, m_view.height,
                                     WINDOW_FORMAT_RGBX_8888);

    m_image_reader = new Image_Reader(&m_view, AIMAGE_FORMAT_YUV_420_888);
    m_image_reader->SetPresentRotation(m_native_camera->GetOrientation());

    ANativeWindow *image_reader_window = m_image_reader->GetNativeWindow();

    m_camera_ready = m_native_camera->CreateCaptureSession(image_reader_window);
}

void CV_Manager::CameraLoop() {
    bool buffer_printout = false;

    // Initialiser l'enregistrement audio
   /*  SetUpAudioRecording();
    StartAudioRecording(); */

    while (1) {
        if (m_camera_thread_stopped) { break; }
        if (!m_camera_ready || !m_image_reader) { continue; }
        m_image = m_image_reader->GetLatestImage();
        if (m_image == nullptr) { continue; }

        ANativeWindow_acquire(m_native_window);
        ANativeWindow_Buffer buffer;
        if (ANativeWindow_lock(m_native_window, &buffer, nullptr) < 0) {
            m_image_reader->DeleteImage(m_image);
            m_image = nullptr;
            continue;
        }

        if (false == buffer_printout) {
            buffer_printout = true;
            LOGI("/// H-W-S-F: %d, %d, %d, %d", buffer.height, buffer.width, buffer.stride,
                 buffer.format);
        }

        m_image_reader->DisplayImage(&buffer, m_image);

        display_mat = cv::Mat(buffer.height, buffer.stride, CV_8UC4, buffer.bits);

        //BarcodeDetect(display_mat);

        ANativeWindow_unlockAndPost(m_native_window);
        ANativeWindow_release(m_native_window);

        //--------Encoder part--------

        BGR2YUV_nv12(display_mat, outOpencv);
        int yPlaneSize = outOpencv.total() * outOpencv.elemSize();
        m_Encode->Encode(outOpencv.data, yPlaneSize);

        ReleaseMats();
    }
    FlipCamera();

}

void CV_Manager::BarcodeDetect(Mat &frame) {
    int ddepth = CV_16S;

    // Convert to grayscale
    cvtColor(frame, frame_gray, CV_RGBA2GRAY);

    // Gradient X
    Sobel(frame_gray, grad_x, ddepth, 1, 0);
    convertScaleAbs(grad_x, abs_grad_x);
    // Gradient Y
    Sobel(frame_gray, grad_y, ddepth, 0, 1);
    convertScaleAbs(grad_y, abs_grad_y);

    // Total Gradient (approximate)
    addWeighted(abs_grad_x, 0.5, abs_grad_x, 0.5, 0, detected_edges);

    // Reduce noise with a 3x3 kernel
    GaussianBlur(detected_edges, detected_edges, Size(3,3), 0, 0, BORDER_DEFAULT);

    // Reducing noise further by using threshold
    threshold(detected_edges, thresh, 120, 255, THRESH_BINARY);

    // Otsu's threshold
    threshold(thresh, thresh, 0, 255, THRESH_BINARY+THRESH_OTSU);

    // Close gaps using a closing kernel
    kernel = getStructuringElement(MORPH_RECT, Size(21,7));
    morphologyEx(thresh, cleaned, MORPH_CLOSE, kernel);

    // Perform erosions and dilations
    erode(cleaned, cleaned, anchor, Point(-1,-1), 4);
    dilate(cleaned, cleaned, anchor, Point(-1,-1), 4);

    // Extract all contours
    findContours(cleaned, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

    // Sort contours in ascending order
    std::sort(contours.begin(), contours.end(), [](const vector<Point>& c1, const vector<Point>& c2) {
        return contourArea(c1, false) < contourArea(c2, false);
    });

    // Draw the largest contour
    drawContours(frame, contours, int(contours.size()-1), CV_GREEN, 2, LINE_8, hierarchy, 0, Point());
}

void CV_Manager::FlipCamera() {
    m_camera_thread_stopped = false;

    // reset info
    if (m_image_reader != nullptr) {
        delete (m_image_reader);
        m_image_reader = nullptr;
    }
    delete m_native_camera;

    if (m_selected_camera_type == FRONT_CAMERA) {
        m_selected_camera_type = BACK_CAMERA;
    } else {
        m_selected_camera_type = FRONT_CAMERA;
    }
    SetUpCamera();
    std::thread loopThread(&CV_Manager::CameraLoop, this);
    loopThread.detach();
}


void CV_Manager::PauseCamera() {
    if (m_native_camera == nullptr) {
        LOGE("Can't flip camera without camera instance");
        return;
    } else if (m_native_camera->GetCameraCount() < 2) {
        LOGE("Only one camera is available");
        return;
    }

    m_camera_thread_stopped = true;
}

void CV_Manager::setUpTCP(){

    const char hostname[] ="172.16.227.15";
    int port2 = 5055;
    m_Client = new SocketClientH264(hostname, port2);
    m_Client -> ConnectToServer();
}

void CV_Manager::SetUpEncoder(){

    // setEncoder(encoder);
    m_Encode = new Encoder(); //Creation class Encoder
    m_Encode->setSocketClientH264(m_Client);
    // calcul bitrate = weight*height*fps (optional)
    m_Encode -> InitCodec(880, 640, 15, 20000); //480,640,15,100000

    if (m_Encode->getStatus() != AMEDIA_OK){
        __android_log_print(ANDROID_LOG_ERROR, "CameraNDK", "Failed to create encoder");
        //std::cout <<"CameraNDK, Failed to create encoder" << std::end1;
        return;
    }

}

void CV_Manager::ReleaseMats() {
    display_mat.release();
    frame_gray.release();
    grad_x.release();
    abs_grad_x.release();
    grad_y.release();
    abs_grad_y.release();
    detected_edges.release();
    thresh.release();
    kernel.release();
    anchor.release();
    cleaned.release();
    hierarchy.release();
}