//
// Created by AL INFOS on 25/01/2024.
//

#ifndef TP_NDK_SOKET_CLIENT_H
#define TP_NDK_SOKET_CLIENT_H
#include <opencv2/core.hpp>
#include "../../../../opencv/OpenCV-android-sdk/sdk/native/jni/include/opencv2/core/mat.hpp"

class SocketClientH264 {
public:
    SocketClientH264(const char* hostname, int port);
    void ConnectToServer();
    void SendImageDims(const int image_rows, const int image_cols);
    void SendImageH264(const uint8_t *pBuf, int size);


private:
    const char* hostname_;
    int port_;
    int pic_num_;
    int socket_fdesc_;

};
#endif //TP_NDK_SOKET_CLIENT_H
