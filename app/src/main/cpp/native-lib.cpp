#include <jni.h>
#include <string>
#include "CV_Manager.h"
#include "android/native_window_jni.h"
#include "android/asset_manager_jni.h"

static CV_Manager app;
extern "C" JNIEXPORT jstring

JNICALL
Java_baf_kuitch_tp_1ndk_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello Farel!!!!!!";
    return env->NewStringUTF(hello.c_str());
}
extern "C"
JNIEXPORT void JNICALL
Java_baf_kuitch_tp_1ndk_MainActivity_setSurface(JNIEnv *env, jobject thiz, jobject surface) {
    // TODO: implement setSurface()
    app.SetNativeWindow(ANativeWindow_fromSurface(env,surface));
    app.SetUpCamera();
    app.setUpTCP();
    app.SetUpEncoder();
    std::thread loopthread(&CV_Manager::CameraLoop, &app);
    loopthread.detach();
}
extern "C"
JNIEXPORT void JNICALL
Java_baf_kuitch_tp_1ndk_MainActivity_releaseCVMain(JNIEnv *env, jobject thiz) {
    // TODO: implement releaseCVMain()`
}
extern "C"
JNIEXPORT void JNICALL
Java_baf_kuitch_tp_1ndk_MainActivity_flipCamera(JNIEnv *env, jobject thiz) {
    app.PauseCamera();
}