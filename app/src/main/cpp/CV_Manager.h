//
// Created by AL INFOS on 23/01/2024.
//

#ifndef TP_NDK_CV_MANAGER_H
#define TP_NDK_CV_MANAGER_H
// Android
#include <android/native_window.h>
#include <jni.h>
// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
// OpenCV-NDK App
#include "Image_Reader.h"
#include "soket_client.h"
#include "Native_Camera.h"
#include "Util.h"
#include "Encoder.h"
// STD Libs
#include <cstdlib>
#include <string>
#include <vector>
#include <thread>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

using namespace cv;
using namespace std;

class CV_Manager {
public:
    const int GetBufferSize() const { return BUFFER_SIZE; }
    const short* GetAudioBuffer() const { return audioBuffer; }
    SLAndroidSimpleBufferQueueItf GetRecorderBufferQueue() const { return recorderBufferQueue; }
    CV_Manager();

    ~CV_Manager();

    CV_Manager(const CV_Manager &other) = delete;

    CV_Manager &operator=(const CV_Manager &other) = delete;

    // sets Surface buffer reference pointer
    void SetNativeWindow(ANativeWindow *native_indow);

    void SetUpCamera();

    void CameraLoop();

    void BarcodeDetect(Mat &frame);

    void ReleaseMats();

    void Flip_Camera(Mat &frame);

    void FlipCamera();

    void PauseCamera();

    void setUpTCP();

    void SetUpEncoder();

    void SetUpAudioRecording();
    void StartAudioRecording();
    void StopAudioRecording();
    void ReleaseAudioRecorder();
private:

    SLObjectItf engineObject = nullptr;
    SLEngineItf engineEngine = nullptr;
    SLObjectItf recorderObject = nullptr;
    SLRecordItf recorderRecord = nullptr;
    SLAndroidSimpleBufferQueueItf recorderBufferQueue = nullptr;
    static constexpr int BUFFER_SIZE = 8192;
    const short audioBuffer[BUFFER_SIZE];
    static constexpr SLuint32 NUM_CHANNELS =  2;
    // holds native window to write buffer too
    ANativeWindow *m_native_window;

    // buffer to hold native window when writing to it
    ANativeWindow_Buffer m_native_buffer;

    // Camera variables
    Native_Camera *m_native_camera;

    //Encoder
    Encoder *m_Encode;

    camera_type m_selected_camera_type = BACK_CAMERA; // Default

    // Image Reader
    ImageFormat m_view{0, 0, 0};
    Image_Reader *m_image_reader;
    AImage *m_image;
    SocketClientH264 *m_Client;
    volatile bool m_camera_ready;

    // OpenCV values
    Mat display_mat;
    Mat frame_gray;
    Mat grad_x;
    Mat abs_grad_x;
    Mat grad_y;
    Mat abs_grad_y;
    Mat detected_edges;
    Mat thresh;
    Mat kernel;
    Mat anchor;
    Mat cleaned;
    Mat hierarchy;
    Mat outOpencv;

    vector<vector<Point>> contours;

    Scalar CV_PURPLE = Scalar(255, 0, 255);
    Scalar CV_RED = Scalar(255, 0, 0);
    Scalar CV_GREEN = Scalar(0, 255, 0);
    Scalar CV_BLUE = Scalar(0, 0, 255);

    bool m_camera_thread_stopped = false;



};
#endif //TP_NDK_CV_MANAGER_H
