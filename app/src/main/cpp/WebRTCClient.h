//
// Created by AL INFOS on 26/02/2024.
//

#ifndef TP_NDK_WEBRTCCLIENT_H
#define TP_NDK_WEBRTCCLIENT_H

#include "api/peer_connection_interface.h"

#include "api/create_peerconnection_factory.h"

class WebRTCClient : public webrtc::PeerConnectionObserver {

public:

    WebRTCClient();

    ~WebRTCClient();

    void InitializePeerConnection();

    void CreateOffer();

    // Override PeerConnectionObserver methods

    virtual void OnIceCandidate(const webrtc::IceCandidateInterface* candidate) override;

    virtual void OnSuccess(webrtc::SessionDescriptionInterface* desc) override;

    // Ajoutez d'autres méthodes de callback nécessaires

private:

    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> peer_connection_factory_;

    rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection_;

};

#endif //TP_NDK_WEBRTCCLIENT_H
