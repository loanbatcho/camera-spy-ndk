#include "WebRTCClient.h"

#include <iostream>

WebRTCClient::WebRTCClient() {

    rtc::InitializeSSL();

    // Créez les threads nécessaires à WebRTC

    network_thread_ = rtc::Thread::CreateWithSocketServer();

    worker_thread_ = rtc::Thread::Create();

    signaling_thread_ = rtc::Thread::Create();

    network_thread_->Start();

    worker_thread_->Start();

    signaling_thread_->Start();

    // Créez la PeerConnectionFactory

    peer_connection_factory_ = webrtc::CreatePeerConnectionFactory(

            network_thread_.get(), worker_thread_.get(), signaling_thread_.get(),

            nullptr, webrtc::CreateBuiltinAudioEncoderFactory(),

            webrtc::CreateBuiltinAudioDecoderFactory(), nullptr, nullptr);

}

WebRTCClient::~WebRTCClient() {

    if (peer_connection_) {

        peer_connection_->Close();

    }

    rtc::CleanupSSL();

}

void WebRTCClient::InitializePeerConnection() {

    if (!peer_connection_factory_) {

        std::cerr << "Erreur lors de la création de la PeerConnectionFactory." << std::endl;

        return;

    }

    webrtc::PeerConnectionInterface::RTCConfiguration config;

    webrtc::PeerConnectionInterface::IceServer server;

    server.uri = "stun:stun.l.google.com:19302";

    config.servers.push_back(server);

    // Créez la PeerConnection

    peer_connection_ = peer_connection_factory_->CreatePeerConnection(

            config, nullptr, nullptr, this);

    if (!peer_connection_) {

        std::cerr << "Erreur lors de la création de PeerConnection." << std::endl;

    }

}

void WebRTCClient::CreateOffer() {

    if (!peer_connection_) {

        std::cerr << "PeerConnection n'est pas initialisée." << std::endl;

        return;

    }

    webrtc::PeerConnectionInterface::RTCOfferAnswerOptions options;

    peer_connection_->CreateOffer(this, options);

}

void WebRTCClient::OnSuccess(webrtc::SessionDescriptionInterface* desc) {

    peer_connection_->SetLocalDescription(DummySetSessionDescriptionObserver::Create(), desc);

    std::string sdp;

    desc->ToString(&sdp);

    // Ici, vous devez envoyer la description de session (SDP) à l'autre pair via votre mécanisme de signaling

    // Par exemple : signalingChannel.send(JSON.stringify({'sdp': sdp}));

}

void WebRTCClient::OnFailure(const std::string& error) {

    std::cerr << "Erreur: " << error << std::endl;

}

void WebRTCClient::OnIceCandidate(const webrtc::IceCandidateInterface* candidate) {

    std::string candidate_str;

    candidate->ToString(&candidate_str);

    // Ici, vous devez envoyer les candidats ICE à l'autre pair via votre mécanisme de signaling

    // Par exemple : signalingChannel.send(JSON.stringify({'candidate': candidate_str}));

}

// DummySetSessionDescriptionObserver est nécessaire pour appeler SetLocalDescription et SetRemoteDescription

class DummySetSessionDescriptionObserver : public webrtc::SetSessionDescriptionObserver {

public:

    static DummySetSessionDescriptionObserver* Create() {

        return new rtc::RefCountedObject<DummySetSessionDescriptionObserver>();

    }

    virtual void OnSuccess() { std::cout << "SetSessionDescriptionObserver::OnSuccess" << std::endl; }

    virtual void OnFailure(webrtc::RTCError error) {

        std::cout << "SetSessionDescriptionObserver::OnFailure " << ToString(error.type()) << ": " << error.message() << std::endl;

    }

};
